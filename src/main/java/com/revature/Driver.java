package com.revature;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Driver {

	public static void main(String[] args) {
		// ac represents the IOC container - Spring's Container which manages our spring beans
		ApplicationContext ac = new ClassPathXmlApplicationContext("beans.xml");
		Customer c = (Customer) ac.getBean("customer");
		System.out.println(c);

		Invoice i = (Invoice) ac.getBean("invoice");
		System.out.println(i);
	}

}
